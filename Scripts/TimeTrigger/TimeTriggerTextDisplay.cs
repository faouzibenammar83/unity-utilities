﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTriggerTextDisplay : MonoBehaviour {

    public string TextString
    {
        get
        {
            return textString;
        }

        set
        {
            textString = value;
        }
    }

    [SerializeField]
    private TimeTrigger trigger = null;

    [SerializeField]
    private UnityEngine.UI.Text text = null;

    [Tooltip("String to append AFTER text.")]
    public string suffix = " seconds";

    [Tooltip("String to append BEFORE text.")]
    public string prefix = "Timming: ";

    [Tooltip("Optional. Time will replace this tag in text")]
    public string replaceTag = "<time>";

    [Tooltip("Format float")]
    public string timeFormat = "00";

    [Tooltip("Minutes and seconds separator")]
    public string separator = ":";

    [Tooltip("To show minutes")]
    public bool showMinutes = true;

    [Tooltip("To show seconds")]
    public bool showSeconds = true;

    [Tooltip("To show miliseconds")]
    public bool showMiliseconds = true;

    [Tooltip("Optional. Text to use as base")]
    public string textString = "";

    public bool useReverseTime = false;


    void OnValidate()
    {
        if (text == null)
        {
            text = GetComponent<UnityEngine.UI.Text>();
        }
        if (trigger == null)
        {
            trigger = GetComponent<TimeTrigger>();
        }

        if (text != null && textString == "")
        {
            textString = text.text;
        }
    }
	
	void Update ()
    {
        if (text == null || trigger == null) return;
        float time = useReverseTime ? trigger.ReverseTime : trigger.Time;
        float timeInt = Mathf.Ceil(time);
        float seconds = Mathf.Ceil(timeInt % 60);
        float minutes = Mathf.Floor(timeInt / 60);
        float miliseconds = Mathf.Ceil(Mathf.Clamp01(timeInt - time) * 100);

        string t = "";

        if (showMinutes)
        {
            t += minutes.ToString(timeFormat);
        }
        if (showSeconds)
        {
            if (showMinutes) t += separator;
            t += seconds.ToString(timeFormat);
        }
        if (showMiliseconds)
        {
            if (showMinutes || showSeconds) t += separator;
            t += miliseconds.ToString(timeFormat);
        }

        if (textString != "")
        {
            t = textString.Replace(replaceTag, t);
        }

        text.text = prefix + t + suffix;
	}
}
