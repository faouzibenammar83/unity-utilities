﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimeTrigger : MonoBehaviour {

    public static TimeTrigger SetTrigger(float time, System.Action callback)
    {
        GameObject timergo = new GameObject("TimeTrigger");
        TimeTrigger trigger = timergo.AddComponent<TimeTrigger>();
        trigger.SetTrigger(time, callback, true);
        return trigger;
    }

    public static TimeTrigger SetTrigger(float time, System.Action<TimeTrigger> updateCallback, System.Action callback)
    {
        TimeTrigger t = SetTrigger(time, callback);
        t.updateCallback = updateCallback;
        return t;
    }

    [System.Serializable]
    public class TimeTriggerEvent : UnityEvent { }

    public TimeTriggerEvent TriggerEvent {
        get
        {
            return triggerEvent;
        }
    }

    [SerializeField]
    private float timeTrigger;

    [SerializeField]
    private bool destroyAfter = false;

    [SerializeField]
    private TimeTriggerEvent triggerEvent = new TimeTriggerEvent();

    private float timeCounter = 0;

    private System.Action<TimeTrigger> updateCallback;


    private void SetTrigger(float time, System.Action callback, bool destroyAfter = false)
    {
        System.Action _callback = callback;
        this.timeTrigger = time;

        this.triggerEvent.AddListener(delegate ()
        {
            if (_callback != null)
            {
                _callback();
            }
        });
        this.destroyAfter = destroyAfter;
    }


    void Update () {
        timeCounter += UnityEngine.Time.deltaTime;
        if (timeCounter >= timeTrigger)
        {
            timeCounter = timeTrigger;

            if (updateCallback != null)
            {
                updateCallback(this);
            }

            if (triggerEvent != null)
            {
                triggerEvent.Invoke();
            }

            enabled = false;

            if (destroyAfter)
            {
                GameObject.Destroy(gameObject);
            }

            return;
        }

        if (updateCallback != null)
        {
            updateCallback(this);
        }
	}

    void OnDestroy()
    {
        triggerEvent = null;
        updateCallback = null;
    }

    public void ResetTrigger()
    {
        timeCounter = 0;
        enabled = true;
    }

    public float Time
    {
        get
        {
            return timeCounter;
        }
    }

    public float ReverseTime
    {
        get
        {
            return timeTrigger - timeCounter;
        }
    }

    public void Pause()
    {
        this.enabled = false;
    }

    public void Resume()
    {
        this.enabled = true;
    }

}
