﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class ServerSync : MonoBehaviour {

    private static ServerSync instance;
    public static ServerSync Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject("Server");
                instance = go.AddComponent<ServerSync>();
                DontDestroyOnLoad(go);
            }

            return instance;
        }
    }

    public static bool IsAlive
    {
        get
        {
            return instance != null;
        }
    }

    public string Ip
    {
        get
        {
            return ip;
        }
    }

    public int Port
    {
        get
        {
            return port;
        }
    }


    public event System.Action<int, string> MessageReceived;
    public event System.Action<int> ClientConnected;

    private List<Message> messageReceived = new List<Message>();
    private List<int> clientConnected = new List<int>();

    private Server server;

    [SerializeField]
    private string ip = "127.0.0.1";

    [SerializeField]
    private int port = 5000;

    public bool loadPortAndIpFromJsonOnStart = false;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("A server already exists");
            Destroy(gameObject);
            return;
        }
        
        instance = this;
    }

    public void SendMessageToClient(string message, int id)
    {
        try
        {
            server.SendMessage(message, id);
        } catch (System.Exception e)
        {
            Debug.LogWarning(e.Message);
        }
    }

    public void BroadCastMessage(string message)
    {
        try
        {
            server.BroadCastMessage(message);
        } catch(System.Exception e)
        {
            Debug.LogWarning(e.Message);
        }
    }

    public void LoadIpFromJson()
    {
        if (loadPortAndIpFromJsonOnStart)
        {
            string json = System.IO.File.ReadAllText("server.json");
            JsonUtility.FromJsonOverwrite(json, this);
        }
    }

    void Start()
    {
        if (loadPortAndIpFromJsonOnStart)
        {
            string json = System.IO.File.ReadAllText("server.json");
            JsonUtility.FromJsonOverwrite(json, this);
        }

        server = new Server(ip, port);
        server.ClientConnected = _ClientConnected;
        server.ReceivedClientMessage = _ReceivedClientMessage;
        server.Start();

        Debug.Log("Server created at address: <b>" + ip + ":" + port + "</b>");
    }

    void Update()
    {
        SyncMessagesReceived();
        SyncClientConnected();
    }

    void OnDestroy()
    {
        server.Close();
    }

    public void Close()
    {
        server.Close();
    }

    private void SyncMessagesReceived()
    {
        if (Monitor.TryEnter(messageReceived, 0))
        {
            try
            {
                if (MessageReceived != null)
                {
                    foreach (Message m in messageReceived)
                    {
                        MessageReceived(m.id, m.message);
                    }
                }
                messageReceived.Clear();
            }
            finally
            {
                Monitor.Exit(messageReceived);
            }
        }
    }

    private void SyncClientConnected()
    {
        if (Monitor.TryEnter(clientConnected, 0))
        {
            try
            {
                if (ClientConnected != null)
                {
                    foreach (int id in clientConnected)
                    {
                        ClientConnected(id);
                    }
                }
                clientConnected.Clear();
            }
            finally
            {
                Monitor.Exit(clientConnected);
            }
        }
    }

    private void _ClientConnected(int clientId)
    {
        Monitor.Enter(clientConnected);
        clientConnected.Add(clientId);
        Monitor.Exit(clientConnected);
    }

    private void _ReceivedClientMessage(int id, string message)
    {
        Monitor.Enter(messageReceived);
        messageReceived.Add(new Message(id, message));
        Monitor.Exit(messageReceived);
    }



    private struct Message
    {
        public Message(int id, string message)
        {
            this.id = id;
            this.message = message;
        }

        public int id;
        public string message;
    }

}
