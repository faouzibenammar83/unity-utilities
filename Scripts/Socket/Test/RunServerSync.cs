﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RunServerSync : MonoBehaviour {

    public Text clientsConnected;
    public Text messageReceived;
    List<Client> clients = new List<Client>();


    void OnDestroy()
    {
        Debug.Log("Shutting down");
        if (ServerSync.IsAlive)
            ServerSync.Instance.Close();

        foreach (Client client in clients)
        {
            client.Close();
        }
        clients.Clear();
    }

    void Start () {

        for (int i = 0; i < 10; ++i)
        {
            Client client = new Client();

            client.messageReceived = delegate (string message)
            {
                Debug.Log("Client Received: " + message);
            };

            client.Connect(ServerSync.Instance.Ip, ServerSync.Instance.Port);
            clients.Add(client);
        }

        ServerSync.Instance.ClientConnected += delegate(int id)
        {
            clientsConnected.text = "Client #" + id.ToString() + " connected";
        };

        ServerSync.Instance.MessageReceived += delegate (int id, string message)
        {
            Debug.Log("Server " + message);
            messageReceived.text = "Client #" + id.ToString("00") + " says:" + message;
        };
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            foreach (Client c in clients) c.SendMessage("oi");
        }
    }
}
