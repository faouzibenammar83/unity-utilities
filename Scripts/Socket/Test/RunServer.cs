﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;
using System.Collections.Generic;

public class RunServer : MonoBehaviour {

    Server server;
    List<Client> clients = new List<Client>();

    IEnumerator Start()
    {
        int port = 11235;
        string serverip = "127.0.0.1";

        Debug.Log(">> New thread");
        server = new Server(serverip, port);

        server.ReceivedClientMessage = delegate (int id, string message)
        {
            Debug.Log("<b>" + id + "</b> => <i>" + message + "</i>");
        };
        server.ClientConnected = delegate (int id)
        {
            Debug.Log("Client <b>" + id + "</b> connected ");
        };
        server.Start();

        yield return new WaitForSeconds(1);

        for (int i = 0; i < 10; ++i)
        {
            Client client = new Client();
             
            client.messageReceived = delegate (string message)
            {
                Debug.Log("Client Received: " + message);
            };

            client.Connect(serverip, port);
            clients.Add(client);
        }
    }

    void OnDestroy()
    {
        Debug.Log("Shutting down");
        server.Close();
        lock (clients)
        {
            foreach (Client client in clients)
            {
                client.Close();
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            //bloqueando aqui pq bloqueia na main thread
            server.BroadCastMessage("oi");
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            int i = 0;
            foreach(Client client in clients)
            {
                ++i;
                client.SendMessage("tudo bom?" + i);
            }
        }
    }
}
