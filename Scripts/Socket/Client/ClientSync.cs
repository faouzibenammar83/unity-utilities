﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine.UI;

public class ClientSync : MonoBehaviour {

    public static string Ip;
    public static int Port;

    public System.Action<string> MessageReceived;
    public System.Action Connected;

    private bool connected = false;
    private List<string> messageReceived = new List<string>();
    private Client client;
	
	virtual protected void Start () {
        string ip = ClientSync.Ip;
        int port = ClientSync.Port;

        client = new Client();
        client.connected = ClientConnected;
        client.messageReceived = MessageReceivedFromServer;

        client.Connect(ip, port);
    }

    virtual protected void SelfConnected()
    {
        //override me
    }

    virtual protected void SelfMessageReceived(string x)
    {
        //override me
    }

    public void SendMessageToServer(string x)
    {
        client.SendMessage(x);
    }

    private void ClientConnected()
    {
        connected = true;
    }

    private void MessageReceivedFromServer(string message)
    {
        Monitor.Enter(messageReceived);
        messageReceived.Add(message);
        Monitor.Exit(messageReceived);
    }

    virtual protected void Update()
    {
        if (connected)
        {
            SelfConnected();
            if (Connected != null)
            {
                Connected();
                connected = false;
            }
        }

        if (Monitor.TryEnter(messageReceived, 0))
        {
            try
            {
                if (messageReceived.Count != 0)
                {
                    if (MessageReceived != null)
                    {
                        foreach (string m in messageReceived)
                        {
                            SelfMessageReceived(m);
                            MessageReceived(m);
                        }
                    } else
                    {
                        foreach (string m in messageReceived)
                        {
                            SelfMessageReceived(m);
                        }
                    }

                    messageReceived.Clear();
                }
            }
            finally
            {
                Monitor.Exit(messageReceived);
            }

        }
    }
}
