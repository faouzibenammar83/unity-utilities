﻿using System.Collections.Generic;
using System.Collections;

public class AStar<T>
{

    #region Delegates

    public delegate float DistanceInformationAboutTwoTiles(T from, T to);
    public delegate T[] GetNeighboor(T t);
    public delegate bool IsWalkable(T t);

    public DistanceInformationAboutTwoTiles HeuristicDistanceDelegate;
    public DistanceInformationAboutTwoTiles DifficultToWalkDelegate;
    public GetNeighboor GetNeighboorDelegate;
    public IsWalkable IsWalkableDelegate;

    #endregion

    #region Variables

    // Used in search method, to avoid deadlock
    private List<T> closed;
    private List<Node> open;

    // Used as auxiliary, the lastNode will contain the information about the whole path
    private Node lastNode;

    // The target, it should go from start to this path
    private T pathTarget;

    #endregion

    #region Public Methods

    ///<summary>
    /// Initializes a new instance of the AStar class.
    /// </summary>

    public AStar()
    {
        this.closed = new List<T>();
        this.open = new List<Node>();
    }

    /// <summary>
    /// Finds a path from start to end.
    /// If it finds some path connecting start to end, returns true; returns false otherwise
    /// The complete path is set on path parameter
    /// If start is already equal to end it returns a path with start in it.
    /// If no path is found, path will be anything not valid.
    /// </summary>

    public bool FindPath(T start, T end, out T[] path)
    {
        if (start.Equals(end))
        {
            path = new T[] { start };
            return true;
        }

        this.pathTarget = end;

        this.open.Clear();
        this.closed.Clear();
        closed.Add(start);

        bool result = Search(new Node(this, start, end));
        path = RecoverPath(lastNode);

        this.closed.Clear();
        this.open.Clear();

        return result;
    }

    #endregion

    #region Private methods

    /// <summary>
    /// From node, recover the possible path;
    /// In an array that represents the path to peform using generic T.
    /// </summary>

    private T[] RecoverPath(Node node)
    {
        List<T> path = new List<T>();
        while (node != null && node.Parent != null)
        {
            path.Add(node.Tile);
            node = node.Parent;
        }

        if (node != null)
            path.Add(node.Tile);

        path.Reverse();
        return path.ToArray();
    }

    /// <summary>
    /// Main method to find the path.
    /// </summary>

    /// Talvez tenha que passar uma lista de nodos abertos (os vizinhos à ser explorado
    /// e todo loop procura pelo vizinho, dentre todos os vizinhos, com o menor F.
    private bool Search(Node currentNode)
    {
        open.AddRange(NodeNeighboors(currentNode, pathTarget, closed));
        open.Sort((node1, node2) => node2.TotalProbableDistance.CompareTo(node1.TotalProbableDistance));

        for (int i = open.Count - 1; i >= 0 && open.Count != 0; --i)
        {
            Node node = open[i];
            open.RemoveAt(i);

            if (closed.Contains(node.Tile)) continue;
            closed.Add(node.Tile);

            if (node.Tile.Equals(pathTarget))
            {
                lastNode = node;
                return true;
            }
            if (Search(node))
            {
                return true;
            }
        }

        return false;
    }

    #endregion

    #region Delegates Method Wrapper

    /// <summary>
    /// Find neighboors of the Tile
    /// </summary>

    protected T[] Neighboors(T tile)
    {
        return GetNeighboorDelegate(tile);
    }

    /// <summary>
    /// Determines whether this node has a tile that is walkable
    /// </summary>

    protected bool IsNodeWalkable(Node search)
    {
        return IsWalkableDelegate(search.Tile);
    }

    /// <summary>
    /// Calculates the real distance from start node to tile node.
    ///
    /// The distance from start to "toTile" is the distance from start to "fromNode" plus the distance from "fromNode" to "toTile".
    /// This method uses 'G' of "fromNode" and calculate distance from "fromNode" to "toTile" using delegate.
    /// </summary>

    protected float CalculateG(Node fromNode, T toTile)
    {
        return fromNode.TotalDistanceFromStart + DifficultToWalkDelegate(fromNode.Tile, toTile);
    }

    /// <summary>
    /// Calculates the heuristic distance from tile "from" to target tile "to"
    /// </summary>

    protected float CalculateH(T from, T to)
    {
        return HeuristicDistanceDelegate(from, to);
    }

    /// <summary>
    /// Gets all neighboors that was not visited yet and are walkable.
    /// Returns neighboors sorted by 'TotalProbableDistance' property.
    /// </summary>

    protected Node[] NodeNeighboors(Node node, T target, List<T> closed)
    {
        List<Node> n = new List<Node>();

        T[] neighboors = Neighboors(node.Tile);
        foreach (T tile in neighboors)
        {
            if (closed.Contains(tile)) continue;
            Node info = new Node(this, tile, target, node);
            if (IsNodeWalkable(info))
            {
                n.Add(info);
            }
        }

        return n.ToArray();
    }

    #endregion

    #region Node Class

    /// <summary>
    /// Represents one node in the path.
    /// Holds information about how hard is to go through this path
    /// And information about how to compose the whole path
    /// </summary>

    protected class Node
    {
        private float totalDistanceFromStart = 0;
        private float heuristicDistance = 0;

        private T tile = default(T);
        private Node parent = null;

        public Node(AStar<T> star, T index, T targetIndex, Node parent = null)
        {
            this.tile = index;
            this.parent = parent;

            if (parent != null)
            {
                this.totalDistanceFromStart = star.CalculateG(parent, this.tile);
            }

            this.heuristicDistance = star.CalculateH(index, targetIndex);
        }

        public Node Parent
        {
            get
            {
                return this.parent;
            }
        }

        public T Tile
        {
            get
            {
                return this.tile;
            }
        }

        public float TotalDistanceFromStart
        {
            get
            {
                return totalDistanceFromStart;
            }
        }

        public float TotalProbableDistance
        {
            get
            {
                return totalDistanceFromStart + heuristicDistance;
            }
        }

        public override bool Equals(object other)
        {
            Node node = (Node)other;
            if (node == null) return false;
            return this.tile.Equals(node.tile);
        }

        public override int GetHashCode()
        {
            return this.tile.GetHashCode();
        }
    }

    #endregion
}