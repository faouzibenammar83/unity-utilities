﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFader : MonoBehaviour {

    #region Static ScreenFader Constructor

    public static ScreenFader Fade(Color from, Color to, float time, Ease ease, System.Action<ScreenFader> callback)
    {
        return CreateFader(from, to, time, ease, callback);
    }

    public static ScreenFader FadeIn(float time, Ease ease, System.Action<ScreenFader> callback)
    {
        return CreateFader(Color.clear, Color.black, time, ease, callback);
    }

    public static ScreenFader FadeOut(float time, Ease ease, System.Action<ScreenFader> callback)
    {
        return CreateFader(Color.black, Color.clear, time, ease, callback);
    }

    private static ScreenFader CreateFader(Color from, Color to, float time, Ease ease, System.Action<ScreenFader> callback)
    {
        GameObject go = new GameObject("ScreenFader");
        ScreenFader fader = go.AddComponent<ScreenFader>();
        fader.from = from;
        fader.to = to;
        fader.time = time;
        fader.ease = ease;
        fader.callback = callback;

        return fader;
    }

    #endregion


    #region Public Properties 

    public float Time
    {
        get { return time; }
        set { time = value; }
    }

    public Ease Ease
    {
        get { return ease; }
        set { ease = value; }
    }

    #endregion

    #region Private Fields

    private Color fadeColor = Color.clear;
    private Texture2D tex = null;
    private Rect pos;
    private Color from = Color.clear;
    private Color to = Color.clear;
    private Ease ease = Ease.Linear;
    private float time = 1;
    private System.Action<ScreenFader> callback;

    private float timer = 0;
    private float delay = 0;
    private bool pause;

    #endregion

    public void Reverse(float delay = 0)
    {
        Reverse(delay, callback);
    }

    public void Reverse(float delay, System.Action<ScreenFader> callback)
    {
        this.callback = callback;

        this.delay = delay;
        Color c = from;
        from = to;
        to = c;
        timer = 0;
    }

    public bool ShouldDestroyWhenFinished { get; set; }

    public void Pause()
    {
        pause = true;
    }

    public void Resume()
    {
        pause = false;
    }


    #region Unity Methods
    void Start ()
    {
        ShouldDestroyWhenFinished = true;
        tex = Resources.Load<Texture2D>("white");
        if (tex == null)
        {
            tex = new Texture2D(2, 2);
            Color[] texColor = new Color[tex.width * tex.height];
            for (int i = 0; i < texColor.Length; ++i)
            {
                texColor[i] = Color.white;
            }
            tex.SetPixels(texColor);
            tex.Apply();
        }
        
        pos = new Rect(0, 0, Screen.width, Screen.height);
    }

    void OnDestroy()
    {
        tex = null;
        callback = null;
    }

    void OnGUI()
    {
        pos.width = Screen.width;
        pos.height = Screen.height;

        Color c = GUI.color;
        GUI.color = fadeColor;
        GUI.DrawTexture(pos, tex, ScaleMode.StretchToFill, true);
        GUI.color = c;
    }

    void Update ()
    {
        if (pause) return;
        if (delay > 0)
        {
            delay -= UnityEngine.Time.deltaTime;
            return;
        }

        timer = Mathf.Min(timer + UnityEngine.Time.deltaTime, time);

        if (timer >= time)
        {
            fadeColor = to;
            if (callback != null)
            {
                callback(this);
            }

            if (timer != 0 && ShouldDestroyWhenFinished)
            {
                Destroy(gameObject);
            }

            return;
        }

        fadeColor = Color.Lerp(from, to, Equations.ChangeFloat(timer / time, 0, 1, 1, ease));
	}

    #endregion
}
