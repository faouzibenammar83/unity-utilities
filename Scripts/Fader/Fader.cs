﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Fader : MonoBehaviour {

    [System.Serializable]
    public class FaderCompleteEvent : UnityEngine.Events.UnityEvent { }


    #region Static Fade Graphic 

    public static void FadeOut(Graphic graphic, float time=1, Ease curveType = Ease.Linear, System.Action<Graphic> del=null)
    {
        Fader f = CreateFader(graphic, curveType, del);
        f.StartCoroutine(f._Fade(graphic.color, Color.clear, time));
    }

    public static void FadeIn(Graphic graphic, float time = 1, Ease curveType = Ease.Linear, System.Action<Graphic> del = null)
    {
        Fader f = CreateFader(graphic, curveType, del);
        f.StartCoroutine(f._Fade(graphic.color, Color.white, time));
    }

    public static void FadeToBlack(Graphic graphic, float time = 1, Ease curveType = Ease.Linear, System.Action<Graphic> del = null)
    {
        Fader f = CreateFader(graphic, curveType, del);
        f.StartCoroutine(f._Fade(graphic.color, Color.black, time));
    }

    public static void FadeToWhite(Graphic graphic, float time = 1, Ease curveType = Ease.Linear, System.Action<Graphic> del = null)
    {
        Fader f = CreateFader(graphic, curveType, del);
        f.StartCoroutine(f._Fade(graphic.color, Color.white, time));
    }

    public static void FadeTo(Graphic graphic, Color color, float time = 1, Ease curveType = Ease.Linear, System.Action<Graphic> del = null)
    {
        Fader f = CreateFader(graphic, curveType, del);
        f.StartCoroutine(f._Fade(graphic.color, color, time));
    }

    public static void Fade(Graphic graphic, Color from, Color to, float time = 1, Ease curveType = Ease.Linear, System.Action<Graphic> del = null)
    {
        Fader f = CreateFader(graphic, curveType, del);
        f.StartCoroutine(f._Fade(from, to, time));
    }

    #endregion

    #region Static Fade Group

    public static void FadeOut(CanvasGroup group, float time = 1, Ease curveType = Ease.Linear, System.Action<CanvasGroup> del = null)
    {
        Fader f = CreateFader(group, curveType, del);
        f.StartCoroutine(f._FadeGroup(group.alpha, 0, time));
    }

    public static void FadeIn(CanvasGroup group, float time = 1, Ease curveType = Ease.Linear, System.Action<CanvasGroup> del = null)
    {
        Fader f = CreateFader(group, curveType, del);
        f.StartCoroutine(f._FadeGroup(group.alpha, 1, time));
    }

    public static void FadeTo(CanvasGroup group, float alpha, float time = 1, Ease curveType = Ease.Linear, System.Action<CanvasGroup> del = null)
    {
        Fader f = CreateFader(group, curveType, del);
        f.StartCoroutine(f._FadeGroup(group.alpha, alpha, time));
    }

    public static void Fade(CanvasGroup group, float fromAlpha, float toAlpha, float time = 1, Ease curveType = Ease.Linear, System.Action<CanvasGroup> del = null)
    { 
        Fader f = CreateFader(group, curveType, del);
        f.StartCoroutine(f._FadeGroup(fromAlpha, toAlpha, time));
    }

    #endregion

    #region Static Factory

    private static Fader CreateFader(Graphic graphic, Ease curveType, System.Action<Graphic> del)
    {
        GameObject go = new GameObject("Fading " + graphic.name);
        Fader f = go.AddComponent<Fader>();
        f.graphic = graphic;
        f.curve = curveType;
        f.FadeComplete = del;

        return f;
    }

    private static Fader CreateFader(CanvasGroup group, Ease curveType, System.Action<CanvasGroup> del)
    {
        GameObject go = new GameObject("Fading " + group.name);
        Fader f = go.AddComponent<Fader>();
        f.group = group;
        f.curve = curveType;
        f.GroupFadeComplete = del;

        return f;
    }

    #endregion


    public FaderCompleteEvent FadeCompleted
    {
        get
        {
            return fadeCompleted;
        }
    }

    #region Fields to use in Inspector only

    [SerializeField]
    public bool destroyOnComplete = true;

    [SerializeField]
    private bool fadeOnStart = false;

    [SerializeField]
    [Range(0,1)]
    private float groupFadeTarget = 0;

    [SerializeField]
    private Color graphicFadeTarget = Color.clear;

    [SerializeField]
    private float time = 1;

    #endregion

    [SerializeField]
    private Ease curve = Ease.Linear;

    [SerializeField]
    private FaderCompleteEvent fadeCompleted = new FaderCompleteEvent();


    private System.Action<CanvasGroup> GroupFadeComplete = null;
    private System.Action<Renderer> RendererComplete = null;
    private System.Action<Graphic> FadeComplete = null;

    private CanvasGroup group;
    private Graphic graphic;
    private Renderer render;

    void OnEnable()
    {
        if (graphic == null) graphic = GetComponent<Graphic>();
        if (group == null) group = GetComponent<CanvasGroup>();
        if (render == null) render = GetComponent<Renderer>();
    }

    void OnValidate()
    {
        if (graphic == null) graphic = GetComponent<Graphic>();
        if (group == null) group = GetComponent<CanvasGroup>();
        if (render == null) render = GetComponent<Renderer>();
    }

    void Awake()
    {
        if (graphic == null) graphic = GetComponent<Graphic>();
        if (group == null) group = GetComponent<CanvasGroup>();
        if (render == null) render = GetComponent<Renderer>();
    }

    void Start()
    {
        if (!fadeOnStart) return;
        Fade();
    }

    void OnDestroy()
    {
        group = null;
        graphic = null;
        render = null;
        GroupFadeComplete = null;
        FadeComplete = null;
    }


    /// <summary>
    /// Starts Fade with Unity Eitor parameters
    /// </summary>
    public void Fade()
    {
        if (group != null)
        {
            Fade(group.alpha, groupFadeTarget, time, null);
            return;
        }

        if (graphic != null)
        {
            Fade(graphic.color, graphicFadeTarget, time, null);
        }

        if (render != null)
        {
            Fade(render.material.color, graphicFadeTarget, time, null);
        }
    }


    /// <summary>
    /// Fade with start and end color, over specific time and optional delegate. This fades over Graphic component
    /// </summary>
    public void Fade(Color start, Color end, float time = 1, System.Action<Graphic> del = null)
    {
        FadeComplete = del;
        graphic = GetComponent(typeof(Graphic)) as Graphic;
        render = GetComponent(typeof(Renderer)) as Renderer;
        StartCoroutine(_Fade(start, end, time));
    }

    /// <summary>
    /// Fade with start and end alpha, over specific time and optional delegate. This fades over CanvasGroup component
    /// </summary>
    public void Fade(float start, float end, float time = 1, System.Action<CanvasGroup> del = null)
    {
        GroupFadeComplete = del;
        group = GetComponent(typeof(CanvasGroup)) as CanvasGroup;
        StartCoroutine(_FadeGroup(start, end, time));
    }

    void InvokeCallback()
    {
        fadeCompleted.Invoke();

        if (FadeComplete != null)
        {
            FadeComplete(graphic);
        }

        if (GroupFadeComplete != null)
        {
            GroupFadeComplete(group);
        }

        if (RendererComplete != null)
        {
            RendererComplete(render);
        }
    }

    IEnumerator _Fade(Color startColor, Color endColor, float time)
    {
        if (time == 0 || (graphic == null && render == null))
        {
            yield break;
        }

        if (graphic != null)
            graphic.color = startColor;
        if (render != null)
            render.material.color = startColor;

        float _timming = 0;
        while (_timming < time)
        {
            _timming = Mathf.Min(_timming + Time.deltaTime, time);
            if (graphic != null)
                graphic.color = Color.Lerp(startColor, endColor, Equations.ChangeFloat(_timming / time, 0, 1, 1, curve));
            if (render != null)
                render.material.color = Color.Lerp(startColor, endColor, Equations.ChangeFloat(_timming / time, 0, 1, 1, curve));                

            yield return null;
        }

        if (graphic != null)
            graphic.color = endColor;
        if (render != null)
            render.material.color = endColor;

        InvokeCallback();

        if (destroyOnComplete)
        {
            Destroy(gameObject);
        }
    }

    IEnumerator _FadeGroup(float start, float end, float time)
    {
        if (time == 0 || group == null)
        {
            yield break;
        }

        group.alpha = start;

        float _timming = 0;
        while (_timming < time)
        {
            _timming = Mathf.Min(_timming + Time.deltaTime, time);
            if (group != null)
                group.alpha = Mathf.Lerp(start, end, Equations.ChangeFloat(_timming / time, 0, 1, 1, curve));
            yield return null;
        }

        group.alpha = end;
        InvokeCallback();

        if (destroyOnComplete)
        {
            Destroy(gameObject);
        }
    }
}
