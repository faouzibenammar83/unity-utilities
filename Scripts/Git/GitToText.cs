﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GitToText : MonoBehaviour
{
    public string hashPrefix;
    public string tagPrefix;
    public string branchPrefix;

    public Text hash;
	public Text branch;
	public Text tagText;


    void UpdateLabels()
    {
        if (hash != null)
        {
            hash.text = hashPrefix + GitInfo.GetCurrentCommitHash();
        }

        if (branch != null)
        {
            branch.text = branchPrefix + GitInfo.GetCurrentBranch();
        }

        if (tagText != null)
        {
            tagText.text = tagPrefix + GitInfo.GetLastTag();
        }
    }

    void OnValidate()
    {
        UpdateLabels();
    }

    void Start ()
    {
        UpdateLabels();
    }
}

