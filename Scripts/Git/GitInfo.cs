﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public class GitInfo {

    private static GitInfoScriptable gitInfo = null;

    public static bool LoadGitInfo()
    {
        if (gitInfo == null)
        {
            gitInfo = UnityEngine.Resources.Load<GitInfoScriptable>("gitInfo");
            if (gitInfo == null)
            {
#if UNITY_EDITOR
                GitInfoScriptable asset = ScriptableObject.CreateInstance<GitInfoScriptable>();
                if (!System.IO.Directory.Exists("Assets/Resources"))
                {
                    System.IO.Directory.CreateDirectory("Assets/Resources");
                }
                UnityEditor.AssetDatabase.CreateAsset(asset, "Assets/Resources/gitInfo.asset");
                UnityEditor.AssetDatabase.SaveAssets();
                gitInfo = asset;
#endif
            }
                
        }

        return gitInfo != null;
    }

	public static string GetCurrentBranch() {
        if (!LoadGitInfo()) return "";

        try
        {
            using (StreamReader reader = new StreamReader(UnityEngine.Application.dataPath + "/../.git/HEAD"))
            {
                string line = reader.ReadLine();
                gitInfo.branch = Path.GetFileName(line.Substring(5));
            }
        }
#pragma warning disable 0168
        catch (System.Exception e)
        {
            gitInfo.branch = "";
        }
#pragma warning restore 0168

        return gitInfo.branch;
        //usually this is the line: "ref: refs/heads/develop"
    }

    public static string GetCurrentCommitHash() {
        if (!LoadGitInfo()) return "";

        string line = "";
		string lastLine = "";

        try
        {
            using (StreamReader reader = new StreamReader(UnityEngine.Application.dataPath + "/../.git/logs/HEAD"))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    lastLine = line;
                }
            }
            gitInfo.hash = lastLine.Split(new string[] { " " }, 3, System.StringSplitOptions.RemoveEmptyEntries)[1];
        }
#pragma warning disable 0168
        catch (System.Exception e) 
        {
            gitInfo.hash = "";
        }
#pragma warning restore 0168

        return gitInfo.hash;
    }

	public static string GetLastTag() {
        if (!LoadGitInfo()) return "";

        string[] remoteTags = GetRemoteTagList ();
		if (remoteTags != null && remoteTags.Length != 0) {
            gitInfo.tag = remoteTags[remoteTags.Length - 1];
		}

        try
        {
            string localTag = GetLastTagFrom(UnityEngine.Application.dataPath + "/../.git/refs/tags/");
            if (localTag != null)
            {
                gitInfo.tag = localTag;
            }
        }
#pragma warning disable 0168
        catch (System.Exception e)
        {
            gitInfo.tag = "";
        }
#pragma warning restore 0168


        return gitInfo.tag;
	}

	private static string GetLastTagFrom(string path) {
		string [] files = Directory.GetFiles (path);
		if (files == null || files.Length == 0) return null;

		System.DateTime lastTime = new System.DateTime (1500, 11, 16);
		string tag = "";

		for (int i = 0; i < files.Length; ++ i) {
			System.DateTime myTime = File.GetCreationTime (files [i]);
			if (myTime > lastTime) {
				lastTime = myTime;
				tag = Path.GetFileName(files[i]);
			}
		}

		if (tag == "")
			return null;

		return tag;
	}

	private static string [] GetLocalTagList() {
		string localPath = UnityEngine.Application.dataPath + "/../.git/refs/tags/";
		string [] files = Directory.GetFiles (localPath);
		for (int i = 0; i < files.Length; ++ i) {
			files[i] = Path.GetFileName(files[i]);
		}
			
		return files;
	}

	private static string [] GetRemoteTagList() {
		List<string> tags = new List<string>();
		string localPath = UnityEngine.Application.dataPath + "/../.git/packed-refs";
		if (!File.Exists (localPath)) return null;

		using (StreamReader reader = File.OpenText(localPath)) {
			string line;
			while ((line = reader.ReadLine()) != null) {
				if (line.Contains("tags")) {
					string [] lineBreak = line.Split(new string[]{" "}, System.StringSplitOptions.RemoveEmptyEntries);
					tags.Add(Path.GetFileName(lineBreak[1]));
				}
			}
		}
			
		return tags.ToArray();
	}
}
