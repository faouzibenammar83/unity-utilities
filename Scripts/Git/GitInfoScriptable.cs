﻿using UnityEngine;
using System.Collections;

public class GitInfoScriptable : ScriptableObject {
    //This FitInfo file is created automatic and should not be removed from its absolute folder.
    public string hash;
    public string branch;
    public string tag;
}
