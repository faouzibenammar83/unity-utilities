﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GitScriptableMenu {

    [MenuItem("Git/Update Git Resource File")]
    public static void UpdateGitInfo()
    {
        if (!GitInfo.LoadGitInfo())
        {
            EditorUtility.DisplayDialog("Error", "Could not Load git information right now.", "Ok");
        } else
        {
            EditorUtility.DisplayDialog("Success", "Current git Branch: " + GitInfo.GetCurrentBranch(), "Ok");
        }
    }

    [MenuItem("Git/Create Git Info GUI")]
    public static void CreateGitInfoGUI()
    {
        GitInfo.LoadGitInfo();
        Debug.Log("Current git Branch: " + GitInfo.GetCurrentBranch());

        GitInfoGUI gui = GameObject.FindObjectOfType<GitInfoGUI>();
        GitInfoPanel guiPanel = GameObject.FindObjectOfType<GitInfoPanel>();

        if (gui == null && guiPanel == null)
        {
            (new GameObject("GitGUI")).AddComponent<GitInfoGUI>();
            return;
        }

        if (guiPanel != null)
        {
            guiPanel.resetLayout = true;
            return;
        }

        Debug.Log("Git info already in place. It might be visible only in game window.");
    }

    [MenuItem("Git/Print Git Info")]
    public static void PrintGitInfo()
    {
        Debug.Log("Current git Tag: " + GitInfo.GetLastTag());
        Debug.Log("Current git Branch: " + GitInfo.GetCurrentBranch());
        Debug.Log("Current git Hash: " + GitInfo.GetCurrentCommitHash());
    }
}
