﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool<T> where T : Component {

    public bool InstantiateObjectWhenEmpty = true;

    private Stack<T> pool = new Stack<T>();
    private T Prefab { get; set; }


    public Pool(T prefab, bool instantiateObjectWhenEmpty = true, bool prewarm = true, int preWarmCount = 10)
    {
        if (prefab == null) throw new System.Exception("Prefab cannot be null");

        Prefab = prefab;
        InstantiateObjectWhenEmpty = instantiateObjectWhenEmpty;
        if (prewarm)
        {
            preWarmCount = Mathf.Max(preWarmCount, 0);
            for(int i = 0; i < preWarmCount; ++i)
            {
                pool.Push(CreateNewObject());
            }
        }
    }

    public virtual void Warm(int count)
    {
        count = Mathf.Max(count, 0);
        for (int i = 0; i < count; ++i)
        {
            pool.Push(CreateNewObject());
        }
    }

    public virtual void Warm(int count, System.Action<float> progress)
    {
        GameObject gofiller = new GameObject("Filler");
        PoolFiller filler = gofiller.AddComponent<PoolFiller>();
        filler.FillPool(this, count, progress);
    }

    public virtual void Clean()
    {
        foreach(T obj in pool)
        {
            GameObject.Destroy(obj.gameObject);
        }

        pool.Clear();
    }

    public virtual T GetObject()
    {
        if (pool.Count == 0)
        {
            if (!InstantiateObjectWhenEmpty) return default(T);
            T newObj = CreateNewObject();
            if (newObj != default(T))
            {
                newObj.gameObject.SetActive(true);
            }

            return newObj;
        }

        T obj = pool.Pop();
        obj.gameObject.SetActive(true);

        return obj;
    }

    public virtual void PutBack(T obj)
    {
        pool.Push(obj);
    }
	
    protected virtual T CreateNewObject()
    {
        T newObj = GameObject.Instantiate(Prefab);
        ObjectPooled objPooled = newObj.gameObject.AddComponent<ObjectPooled>();
        objPooled.SetPool<T>(this, newObj);
        newObj.gameObject.SetActive(false);

        return newObj;
    }
}
