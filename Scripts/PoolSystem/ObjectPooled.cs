﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooled : MonoBehaviour {

    private interface IPoolWrap
    {
        void Disabled();
        void Dispose();
    }

    private struct PoolWrap<T> : IPoolWrap where T : Component
    {
        public Pool<T> Pool { get; set; }
        public T objectPooled { get; set; }

        public void Disabled()
        {
            Pool.PutBack(objectPooled);
        }

        public PoolWrap(Pool<T> p, T obj)
        {
            this.Pool = p;
            this.objectPooled = obj;
        }

        public void Dispose()
        {
            this.Pool = null;
            this.objectPooled = null;
        }
    }

    private IPoolWrap wrap;

    public void SetPool<T>(Pool<T> p, T obj) where T : Component
    {
        wrap = new PoolWrap<T>(p, obj);
    }

    private void OnDisable()
    {
        wrap.Disabled();
    }

    private void OnDestroy()
    {
        wrap.Dispose();
        wrap = null;
    }




    /// <summary>
    /// TESTIJNF
    /// </summary>
    void Deactivate()
    {
        StartCoroutine(_Deac());
    }

    IEnumerator _Deac()
    {
        yield return new WaitForSeconds(1.5f);
        gameObject.SetActive(false);
    }
}
