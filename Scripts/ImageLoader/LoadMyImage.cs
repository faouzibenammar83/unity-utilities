﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LoadMyImage : MonoBehaviour {

    public string path;
    public string url;
    public Image image;
    public MeshRenderer render;
    public SpriteRenderer sprite;

    [System.Serializable]
    public class ImageLoadedEvent : UnityEvent { }

    [SerializeField]
    private ImageLoadedEvent imageLoaded;

    public ImageLoadedEvent ImageLoaded
    {
        get
        {
            return imageLoaded;
        }
    }

    private void Awake ()
    {
        OnValidate();
    }

    private void Start()
    {
        if (path != "")
        {
            SetTexture(ImageLoader.LoadTextureFromFile(path));
        }
        if (url != "")
        {
            ImageLoader.LoadTextureFromUrl(url, delegate (Texture2D tex)
            {
                SetTexture(tex);
            });
        }
    }

    private void SetTexture(Texture2D tex)
    {
        if (image != null)
        {
            image.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        }
        if (render != null)
        {
            render.material.mainTexture = tex;
        }
        if (sprite != null)
        {
            sprite.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        }

        if (imageLoaded != null)
        {
            imageLoaded.Invoke();
        }
    }

    private void OnValidate()
    {
        if (image == null)
            image = GetComponent(typeof(Image)) as Image;
        if (render == null)
            render = GetComponent<MeshRenderer>();
        if (sprite == null)
            sprite = GetComponent<SpriteRenderer>();
    }
}
