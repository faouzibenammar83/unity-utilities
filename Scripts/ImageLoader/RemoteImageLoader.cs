﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteImageLoader : MonoBehaviour {

    public static void LoadSpriteFromUrl(string url, Rect rect, Vector2 pivot, System.Action<Sprite> callback)
    {
        GameObject go = new GameObject("ImageLoader");
        RemoteImageLoader x = go.AddComponent<RemoteImageLoader>();
        x._LoadSpriteFromUrl(url, callback, rect, pivot);
    }

    public static void LoadSpriteFromUrl(string url, System.Action<Sprite> callback)
    {
        GameObject go = new GameObject("ImageLoader");
        RemoteImageLoader x = go.AddComponent<RemoteImageLoader>();
        x._LoadSpriteFromUrl(url, callback);
    }

    public static void LoadTextureFromUrl(string url, System.Action<Texture2D> callback)
    {
        GameObject go = new GameObject("ImageLoader");
        RemoteImageLoader x = go.AddComponent<RemoteImageLoader>();
        x._LoadTextureFromUrl(url, callback);
    }

    private void _LoadSpriteFromUrl(string url, System.Action<Sprite> callback, Rect rect, Vector2 pivot)
    {
        StartCoroutine(_LoadTextureFromUrlRoutine(url, delegate (Texture2D tx)
        {
            if (callback != null)
            {
                Sprite s = Sprite.Create(tx, rect, pivot);
                callback(s);
            }
        }));
    }

    private void _LoadSpriteFromUrl(string url, System.Action<Sprite> callback)
    {
        StartCoroutine(_LoadTextureFromUrlRoutine(url, delegate (Texture2D tx)
        {
            if (callback != null)
            {
                Sprite s = Sprite.Create(tx, new Rect(0, 0, tx.width, tx.height), new Vector2(0.5f, 0.5f));
                callback(s);
            }
        }));
    }

    private void _LoadTextureFromUrl(string url, System.Action<Texture2D> callback)
    {
        StartCoroutine(_LoadTextureFromUrlRoutine(url, callback));
    }

    private IEnumerator _LoadTextureFromUrlRoutine(string url, System.Action<Texture2D> callback)
    {
        System.Action<Texture2D> _callback = callback;

        WWW www = new WWW(url);
        yield return www;

        if (www.error != null && www.error != "")
        {
            Debug.LogWarning("Error while loading image at path: " + url + " ERROR: " + www.error);
            if (callback != null) callback(null);
            yield break;
        }

        Texture2D tex = new Texture2D(2, 2);
        www.LoadImageIntoTexture(tex);
        tex.Apply();
        tex.name = url;

        if (_callback != null)
        {
            _callback(tex);
        }

        Destroy(gameObject);
    }
}
