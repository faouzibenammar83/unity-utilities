﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(UnityTagAttribute))]
public class UnityTagDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.stringValue == "") property.stringValue = "Untagged";
        property.stringValue = EditorGUI.TagField(position, label, property.stringValue);
    }
}
