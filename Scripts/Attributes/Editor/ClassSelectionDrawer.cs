﻿using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[CustomPropertyDrawer(typeof(ClassSelectionAttribute))]
public class ClassSelectionDrawer : PropertyDrawer {

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
        ClassSelectionAttribute flagSettings = (ClassSelectionAttribute)attribute;
        
        System.Type baseType = fieldInfo.FieldType;

        if (!(typeof(UnityEngine.Object).IsAssignableFrom(baseType)))
        {
            Debug.LogError("ClassSelectionAttribute, at the moment, is only aplied to Unity's objects");
            return;
        }
        
        Type[] results = ReflectionUtil.GetTypesInheritanceFrom(baseType);

        List<string> classesToSelect = new List<string>();
        List<int> indexes = new List<int>();

        classesToSelect.Add("Null");
        indexes.Add(0);

        int index = indexes.Count;
        int selected = 0;
        foreach (Type t in results)
        {
            classesToSelect.Add(t.Name);

            if (property.objectReferenceValue != null && property.objectReferenceValue.GetType() == t)
            {
                selected = index;
            }

            indexes.Add(index++);
        }

        int currentSelection = selected;
        selected = EditorGUI.IntPopup(position, label.text, selected, classesToSelect.ToArray(), indexes.ToArray());
        if (selected == currentSelection) return;
        GameObject.DestroyImmediate(property.objectReferenceValue);
        property.objectReferenceValue = null;

        MonoBehaviour mono = property.serializedObject.targetObject as MonoBehaviour;
        if (mono == null)
        {
            Debug.LogError("Shouldn't this be placed on a gameObject?");
            return;
        }
        GameObject go = mono.gameObject;

        if (selected != 0)
        {
            Type selectedType = results[selected - 1];
            if (selectedType != null)
            {
                property.objectReferenceValue = go.AddComponent(selectedType);
                property.serializedObject.ApplyModifiedProperties();
            }       
        }

        EditorGUIUtility.ExitGUI();
    }

}