﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtension {
    public static T FindInParents<T>(this GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null)
            return comp;

        Transform t = go.transform.parent;
        while (t != null && comp == null)
        {
            comp = t.gameObject.GetComponent<T>();
            t = t.parent;
        }
        return comp;
    }

    public static T FindInDeepChildren<T>(this GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null)
            return comp;

        foreach(Transform t in go.transform)
        {
            comp = t.gameObject.FindInDeepChildren<T>();
            if (comp != null) return comp;
        }

        return comp;
    }
}