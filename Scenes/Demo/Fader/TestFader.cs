﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestFader : MonoBehaviour {

    public CanvasGroup group;

	void Start () {
        Fader.FadeIn(group, 2, Ease.Linear, delegate (CanvasGroup c)
        {
            Debug.Log("CanvasGroup " + c.name + " has finished to fade");
        });
	}
}
