﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFadeTest : MonoBehaviour {
    public void StartFading()
    {
        ScreenFader.FadeIn(1, Ease.Linear, delegate (ScreenFader f)
        {
            f.Reverse(0.5f, null);
            //f.Pause();
            //do something behind scenes
            //f.Resume();
        });
    }
}
