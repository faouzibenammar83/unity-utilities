﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadImageFromLocal : MonoBehaviour {

    public string localImageFile;
    public string localImageFolder;

    public MeshRenderer loadImageInto;
    public MeshRenderer [] loadImagesInto;
    public Image image;

	void Start () {
        loadImageInto.material.mainTexture = ImageLoader.LoadTextureFromFile(localImageFile);
        image.sprite = ImageLoader.LoadSpriteFromFile(localImageFile);

        //This is for testing multiple loads
        //It is expecting to load only one texture for each file in the folder
        for (int i = 0; i < loadImagesInto.Length; ++i)
        {
            int index = i;
            MeshRenderer r = loadImagesInto[index];

            ImageLoader.LoadAllTexturesFromFolderAsync(localImageFolder, false, delegate (Texture2D[] texs)
            {
                if (texs == null)
                {
                    Debug.LogError("Could not load images from folder " + localImageFolder);
                    return;
                }

                index = Mathf.Clamp(index, 0, texs.Length);
                r.material.mainTexture = texs[index];

            }, delegate (float progress)
            {
                Debug.Log("[" + index + "]: " + progress);
            });
        }
	}
}
