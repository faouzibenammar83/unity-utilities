﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadImageButton : MonoBehaviour {

    public Image img;
    public string file;

	public void Load()
    {
        img.sprite = ImageLoader.LoadSpriteFromFile(file);
    }
}
