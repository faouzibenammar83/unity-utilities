﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassA : ClassBase
{
    [Range(0,1)]
    public float range;
    public override void Foo()
    {
        Debug.Log("A");
    }
}
