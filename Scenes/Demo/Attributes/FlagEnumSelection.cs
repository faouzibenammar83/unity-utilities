﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagEnumSelection : MonoBehaviour {

    [Flags]
    public enum MyEnum
    {
        Flag1 = 0x1,
        Flag2 = 0x2,
        Flag3 = 0x4,
        Flag4 = 0x8,
        Flag5 = 0x10
    }

    [EnumFlag]
    public MyEnum select;

    private void OnValidate()
    {
        if ((select & MyEnum.Flag1) == MyEnum.Flag1)
        {
            Debug.Log("Flag1 selected");
        }

        if ((select & MyEnum.Flag2) == MyEnum.Flag2)
        {
            Debug.Log("Flag2 selected");
        }

        if ((select & MyEnum.Flag3) == MyEnum.Flag3)
        {
            Debug.Log("Flag3 selected");
        }

        if ((select & MyEnum.Flag4) == MyEnum.Flag4)
        {
            Debug.Log("Flag4 selected");
        }

        if ((select & MyEnum.Flag5) == MyEnum.Flag5)
        {
            Debug.Log("Flag5 selected");
        }
    }
}
