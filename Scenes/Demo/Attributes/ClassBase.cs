﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// At this moment to use "ClassSelectionAttribute" we have to use "MonoBehaviour"
/// </summary>
public abstract class ClassBase : MonoBehaviour, InterfaceA
{
    public int value;
    public abstract void Foo();
}
