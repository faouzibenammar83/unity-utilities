This is my personal utility box for [Unity3D](https://unity3d.com) and you are welcome to use ;)

To use it you may Copy the repository (where the main folders are Scripts and Resources. The others are for testing and examples) to your Assets folder.
Or you may use it as submodule, cloning it inside your Assets folder.
In the future, when the project is more mature, we may make avaliable a package for easy importing.

* You can download a copy of the repository through this [link](https://gitlab.com/z3r0_th/unity-utilities/repository/archive.zip?ref=master). 
Download the zip, and unzip it at Assets/unity-utility folder

* You can submodule it. With your project already versionated with git, just do:

```
git submodule add https://gitlab.com/z3r0_th/unity-utilities.git Assets/unity-utilities
```

It will clone the project into Assets/unity-utilities.
Then, when cloning from other machine you can do:

```
git clone your project
git submodule init
git submodule update
```

* If you wish to clone the repository just to test or to contribute you can create an empty Unity project and clone unity-utility inside the Assets folder.

</br>
Note that this README is somehow incomplete at the moment. We are hilighting some features, but more methods and features can be found inside the project. 
This is a work in progress and in the future we may release a more complete documentation.

</br>
Note too that the examples in "Scenes" are examples and tests and are not optimized and some are "wrongly" wrinten on porpose to force some behaviour 
or to be more clear the scripts porpouse etc. You should NOT use it as guid-line in this moment. 

Thankyou :)

</br>

<h2>GitInfo</h2>

![GitInfo Panel Screenshot](/ReadmeImages/gitinfo.png)

This should give you the ability to put in the project the current git 
information. Useful to track information about the build. </br>

It creates a scriptable object and uses it to keep track of git information. 
You may add or update git information from menu "Git". </br>

It has some scripts to help to show information:</br>

* ```GitInfoPanel``` uses uGUI.

* ```GitInfoGUI``` uses old unity GUI format

* ```GitToText``` uses uGUI text to populate with git information

if you want to use in your code you may use-:

* ```GitInfo.GetLastTag() ```

* ```GitInfo.GetCurrentBranch() ```

* ```GitInfo.GetCurrentCommitHash() ```

</br>

<h1>Image Loader</h1>

Image loader helps to load texture and sprites from remote server or from local machine.
It caches and avoids to load same image multiple times.

You can use ```ImageLoader``` as entry point to load any image. Or you can 
use ```LoadMyImage```, a monobehaviour to load image into itself, editing in inspector

You may use ```ImageLoader``` as:

</br>
* *Load texture or sprite from remote*

```csharp
ImageLoader.LoadTextureFromUrl(url, delegate(Texture2D tex) { /*implement callback*/ });
ImageLoader.LoadSpriteFromUrl(url, delegate(Sprite spr) { /*implement callback*/ });
```

* *Load texture from local file*

```csharp
Texture2D tex = ImageLoader.LoadTextureFromFile(path);
Sprite spr = ImageLoader.LoadTextureFromFile(path);
```

* *Load all textures from local folder*

Sync:
```csharp
Texture2D [] textures = LoadAllTexturesFromFolder(folderPath, isRecursive);
```
Async:
```csharp
LoadAllTexturesFromFolderAsync(folderPath, isRecursive, delegate(Texture2D [] textures) {}, delegate(float progress) {} );
```

<h1>Fader</h1>

![Fader Example Demo](/ReadmeImages/unity_fader.gif)

Fader helps to fade in or fade out a UI.Graphic (Image, for example) or a 
UI.CanvasGroup

You just call the Fader. You may implement a callback to when it has finished. 
It may contaim an Ease Curve.

There is a trigger that is fired when the fade completes

```csharp
Fader.FadeOut(image, time, delegate(var img) {Debug.Log("Finished");});
```

<h1>TimeTrigger</h1>

Time Trigger is a way to trigger an event after certain time and keep control and access to it. 
You may use it to count the of a Match on some game. Or the cooldown time etc.

You can easely do:

```
//After 5 seconds, the callback is called.
//After 5 seconds, the console shows "Time Finished"
TimeTrigger timer = TimeTrigger.SetTrigger(5, delegate ()
{
    Debug.Log("Time Finished");
});

//or if you wish to get the update:
TimeTrigger timer = TimeTrigger.SetTrigger(1, delegate (TimeTrigger timer)
{
    Debug.Log("Time Update: " + timer.Time);
}, delegate ()
{
    Debug.Log("Time Finished");
});

//Note that either way you got the TimerTrigger Object to check it's properties (Time and ReverseTime)
//or call ResetTrigger, Pause or Resume
```

You can use the MonoBehaviour as well, configuring the timer trigger on inspector.

<h1>Path Finding</h1>

<h2>AStar (A*)</h2>
![Path Finding with AStar example](/ReadmeImages/unity_astar_pathfinding.gif)

This AStar is a very generic porpuse algorithm to find paths in a system. 
For example, you may create a grid and find the best path between two points.

Once you have your path system configured, you can:

```csharp

//Note that it can be anything - A GridCell object, for example
star = new AStar<GameObject>();
 
//How hard it is to move from one point to another
//You can increase the difficulty if you are walking over a mountain, for example
star.DifficultToWalkDelegate = delegate(GameObject from, GameObject to) {
      return 1;
};
 
//Is this object walkable? Return true if it is able to be a path
//You may return false if it is the edge of level, or something is over the path
//a rock, an enemy etc.
star.IsWalkableDelegate = delegate(GameObject t) {
      return t.tag != "blocked";
};
 
//Educated guess about the distance. Usually it is a streight line.
star.HeuristicDistanceDelegate = delegate(GameObject from, GameObject to) {
      return Vector3.Distance(from.transform.position, to.transform.position);
};
 
//Who is the neighbor of this object? If it is a grid you may have 
//four (up,down,left,right) and optional diagonals 
//But it can have other configurations
star.GetNeighboorDelegate = delegate(GameObject t) {
      int index = int.Parse(t.name);
      return neighboorsOf(index);
};

//To find a possible path you do:
GameObject [] path;
if (astar.FindPath(from, to, out path)) {
    //Path *FOUND*!
} else {
    //Path *NOT* found!
}
```

[First written here](https://doctorzeroth.wordpress.com/2016/12/04/path-find-astar/) - my personal blog (Portuguese pt-br)

<h1>Attributes</h1>

<h2>ClassSelectionAttribute</h2>

![Select Enum Attribute Screenshot](/ReadmeImages/unity_strategy_selector.gif)
![Code example](/ReadmeImages/strategySelect_code.png)

This allows you to create a base, Monobehaviour, class and select the implementation from Editor. 
Useful to let the game designer choose a certain behaviour to that gameObject and, at same time, allow other programmers to just extend the behaviour.

For example, you may create a script to represent an enemy ship chase behaviour:
"ShipChase", and than extend it: ShipStreightChase, ShipAvoidBulletsChase, 
ShipFlankChase, etc. As you add new behaviours more will be avaliable in 
editor to choose.

Game designer will, than, choose the right behaviour in the editor. 
It is an attribute and you can use like this:

```csharp
[ClassSelection]
public ClassBase selectInterface1;
```

</br>
<h2>EnumFlagAttribute</h2>

![Select Enum Attribute Screenshot](/ReadmeImages/enumselect.png)

This attribute allows multiple enum flag selection from editor. You may use as:

```csharp
[Flags]
public enum MyEnum
{
    Flag1 = 0x1,
    Flag2 = 0x2,
    Flag3 = 0x4,
    Flag4 = 0x8
}

[EnumFlag]
public MyEnum select;
```
and than test the flag variable:

```csharp
if ((select & MyEnum.Flag1) == MyEnum.Flag1)
{
    Debug.Log("Flag1 selected");
}
```

</br>
<h2>UnityTagAttribute</h2>

![Select Unity Tag](/ReadmeImages/tagselection.png)

This allows user to select a tag from inspector, instead of writing the string directly. Use as:

```csharp
[UnityTag]
public string selectTag
```
